
function atras() {
    // Esta funciono realiza la misma acción que el botón atras
    window.history.back();
}

function agregar_url(estado, titulo, direccion) {
    direccion = './' + direccion;
    window.history.pushState(estado, titulo, direccion);
}

function hacer_foco(elemento) {
    // Esta función realiza el foco en el elemeto dado
    $(elemento).focus();
}

function abrir_menu() {
    // Al tocar el botón para abrir el menú
    // Obtengo si el menú ya estaba visible o no
    var menu_visible = $('.collapse').is(':visible');

    // Si el menú no estaba visible
    if(!menu_visible) {
        // Seteo en la URL un # con el estado 'ver_menu'
        agregar_url('ver_menu', null, '#')
    } else {
        // Si el menú ya estaba visible
        var estado_url = window.history.state;
        // Me fijo si estoy en la URL 'ver_menu' con un #
        if(estado_url == 'ver_menu') {
            // Voy uno para atrás para sacar el # y cerrar el menú
            atras();
        }
    }
}
