
// Este evento se ejecuta cuando se cargó toda la página
$(document).ready(function() {

    console.log('cargo la pagina, hora: ' + $.now());

    // Detecto los eventos de la ventana del navegador
    if(window.history && window.history.pushState) {
        // Si se presiona hacia atrás
        $(window).on('popstate', function() {

            console.log('voy hacia atras');

            // Si algún modal está abierto
            if($('div.modal').is(':visible')) {
                // Lo oculto
                $('div.modal').modal('hide');
            }

            // Si el menú está visible
            if($('.collapse').is(':visible')) {
                // Lo oculto
                $('.collapse').collapse('hide');
            }
        });
    }

    // Cuando algún modal se abra
    $('div.modal').on('show.bs.modal', function(e) {
        // Seteo en la URL un # con el estado 'ver_modal'
        agregar_url('ver_modal', null, '#')
    });

    // Cuando algún modal se cierra
    $('div.modal').on('hidden.bs.modal', function() {
        // Me fijo el estado de la URL
        var estado_url = window.history.state;
        // Si el estado es 'ver_modal'
        if(estado_url == 'ver_modal') {
            // Voy uno para atrás para sacar el # y cerrar el modal
            atras();
        }
    });

});

// Este evento detecta todos los clicks
$(document).click(function(e) {

    console.log('hago click');

    // Si el menú ya estaba visible
    if($('.collapse').is(':visible')) {
        // Me fijo si estoy en la URL 'ver_menu' con un #
        var estado_url = window.history.state;
        if(estado_url == 'ver_menu') {
            // Voy uno para atrás para sacar el # y cerrar el menú
            atras();
        }
    }
});
